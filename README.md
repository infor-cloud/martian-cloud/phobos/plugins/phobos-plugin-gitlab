# Phobos Plugin GitLab

This Phobos GitLab Plugin allows interacting with GitLab via a Phobos pipeline.

### Usage Example

```hcl
variable "gitlab_access_token" {
  type = string
}

variable "gitlab_api_url" {
  type = string
}

variable "gitlab_project_path" {
  type = string
}

variable "secret_token" {
  type = string
}

plugin gitlab {
  api_url = var.gitlab_api_url
  token   = var.gitlab_access_token
}

stage test {
  task "launch_job" {
    action "gitlab_run_job" "start_manual_job" {
      project_path = var.gitlab_project_path
      ref          = "feature/delete-resource"
      job_name     = "test"
      commit_id    = "1234567890"
      environment_variables = {
        "JOB_SLEEP_INTERVAL"  = "60s"
      }
      file_variables = {
        "SECRET_TOKEN" = var.secret_token
      }
    }
  }
}
stage deploy {
  task "create_pipeline" {
    action "gitlab_create_pipeline" "deployment_pipeline" {
      project_path = var.gitlab_project_path
      ref          = "feature/add-resource"
      environment_variables = {
        "CONNECTION_RETRIES"  = 5
      }
      file_variables = {
        "SECRET_TOKEN" = var.secret_token
      }
    }
  }
}
```
