vcs_token "my_vcs_token" {
  provider_id = "my-vcs-provider"
}

plugin_requirements {
  gitlab = {
    source = "<<org>>/gitlab"
  }
}

variable "gitlab_api_url" {
  type = string
}

variable "gitlab_project_path" {
  type = string
}

variable "gitlab_ref" {
  type    = string
  default = "main"
}

plugin "gitlab" {
  api_url   = var.gitlab_api_url
  token     = vcs_token.my_vcs_token.value
}

stage "deploy" {
  task "launch_job" {
    action "gitlab_run_job" {
      ref          = var.gitlab_ref
      project_path = var.gitlab_project_path
      job_name     = "Manual Job"
      environment_variables = {
        "FOO" = "bar"
      }
    }
  }
}
