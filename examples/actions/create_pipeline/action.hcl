action "gitlab_create_pipeline" {
  project_path = var.gitlab_project_path
  ref          = var.gitlab_ref
  environment_variables = {
    "FOO"  = "bar"
  }
}
