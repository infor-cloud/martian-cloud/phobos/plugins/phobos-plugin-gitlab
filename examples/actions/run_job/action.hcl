action "gitlab_run_job" {
  ref          = var.gitlab_ref
  project_path = var.gitlab_project_path
  job_name     = "Manual Job"
  environment_variables = {
    "FOO"  = "bar"
  }
}
