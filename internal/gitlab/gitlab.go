// Package gitlab contains a GitLab client
package gitlab

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/aws/smithy-go/ptr"
	"github.com/dustin/go-humanize"
	"github.com/hashicorp/go-hclog"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	defaultAPIURL         = "https://gitlab.com/api/v4/"
	maxPollingAttempts    = 10
	logPollingInterval    = 5 * time.Second
	statusPollingInterval = 10 * time.Second
)

// AuthType is the type of authentication to use
type AuthType string

// Supported authentication types
const (
	AccessToken AuthType = "access_token"
	OAuthToken  AuthType = "oauth_token" //nolint Not a credential
)

// Options are the options for creating a GitLab client
type Options struct {
	Version        string
	Logger         hclog.Logger
	APIURL         *string
	TokenRetriever func() (string, error)
	AuthType       AuthType
}

// RunJobInput is the input data for the RunJob function
type RunJobInput struct {
	CommitID             *string
	ProjectPath          *string
	ProjectID            *int
	EnvironmentVariables map[string]string
	FileVariables        map[string]string
	Ref                  string
	JobName              string
}

// CreatePipelineInput is the input data for the CreatePipeline function
type CreatePipelineInput struct {
	ProjectPath          *string
	ProjectID            *int
	EnvironmentVariables map[string]string
	FileVariables        map[string]string
	Ref                  string
}

// Client is a GitLab client
type Client struct {
	logger         hclog.Logger
	apiClient      *gitlab.Client
	tokenRetriever func() (string, error)
	apiURL         string
	authType       gitlab.AuthType
}

// New creates a new GitLab client
func New(options *Options) (*Client, error) {
	apiURL := defaultAPIURL
	if options.APIURL != nil {
		apiURL = *options.APIURL
	}

	token, err := options.TokenRetriever()
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve auth token: %w", err)
	}

	var (
		apiClient *gitlab.Client
		authType  gitlab.AuthType
	)

	switch options.AuthType {
	case AccessToken:
		apiClient, err = gitlab.NewClient(
			token,
			gitlab.WithBaseURL(apiURL),
		)
		authType = gitlab.PrivateToken
	case OAuthToken:
		apiClient, err = gitlab.NewOAuthClient(
			token,
			gitlab.WithBaseURL(apiURL),
		)
		authType = gitlab.OAuthToken
	default:
		return nil, fmt.Errorf("unsupported auth type: %s", options.AuthType)
	}

	if err != nil {
		return nil, fmt.Errorf("failed to create GitLab client: %w", err)
	}

	return &Client{
		logger:         options.Logger,
		apiClient:      apiClient,
		apiURL:         apiURL,
		tokenRetriever: options.TokenRetriever,
		authType:       authType,
	}, nil
}

// RunJob runs a GitLab CI job
func (c *Client) RunJob(ctx context.Context, input *RunJobInput, ui terminal.UI) (*gitlab.Job, error) {
	projectIdentifier, err := c.getProjectIdentifier(input.ProjectPath, input.ProjectID)
	if err != nil {
		return nil, err
	}

	token, err := c.tokenRetriever()
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve auth token: %w", err)
	}

	pipelines, listPipelinesResponse, err := c.apiClient.Pipelines.ListProjectPipelines(
		projectIdentifier,
		&gitlab.ListProjectPipelinesOptions{
			ListOptions: gitlab.ListOptions{
				// We only care about the "latest" pipeline, incase there are multiple.
				PerPage: 1,
				Page:    1,
			},
			SHA:     input.CommitID,
			Ref:     &input.Ref,
			OrderBy: ptr.String("id"),
			Sort:    ptr.String("desc"),
		},
		gitlab.WithContext(ctx),
		gitlab.WithToken(c.authType, token),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to get GitLab CI pipelines: %w", err)
	}
	defer listPipelinesResponse.Body.Close()

	if listPipelinesResponse.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("list pipelines returned unexpected status code: %d", listPipelinesResponse.StatusCode)
	}

	if len(pipelines) == 0 {
		return nil, fmt.Errorf("no pipelines found for ref %s", input.Ref)
	}

	pipelineID := pipelines[0].ID

	job, err := c.getPipelineJob(ctx, projectIdentifier, pipelineID, input.JobName)
	if err != nil {
		return nil, err
	}

	if job.FinishedAt != nil {
		ui.Output("Retrying GitLab CI job %q for pipeline %d", job.Name, pipelineID, terminal.WithInfoStyle())

		retryToken, cErr := c.tokenRetriever()
		if cErr != nil {
			return nil, fmt.Errorf("failed to retrieve auth token: %w", cErr)
		}

		// Retry finished (failed, canceled, etc.) job.
		retriedJob, retryJobResponse, retryErr := c.apiClient.Jobs.RetryJob(
			projectIdentifier,
			job.ID,
			gitlab.WithContext(ctx),
			gitlab.WithToken(c.authType, retryToken),
		)
		if retryErr != nil {
			return nil, fmt.Errorf("failed to retry GitLab CI job: %w", retryErr)
		}
		defer retryJobResponse.Body.Close()

		if retryJobResponse.StatusCode != http.StatusCreated {
			return nil, fmt.Errorf("retry job returned unexpected status code: %d", retryJobResponse.StatusCode)
		}

		finalJob, oErr := c.outputJobLogs(ctx, ui, projectIdentifier, retriedJob.ID)
		if oErr != nil {
			if ctx.Err() != nil {
				// Cancel the job as the context was canceled.
				if cErr := c.handleJobCancellation(projectIdentifier, retriedJob.ID, ui); cErr != nil {
					return nil, cErr
				}

				return nil, ctx.Err()
			}

			return nil, oErr
		}

		return finalJob, nil
	}

	if job.Status == "created" {
		ui.Output("Waiting for GitLab CI job %q for pipeline %d to be ready", job.Name, pipelineID, terminal.WithInfoStyle())

		// Wait until job is ready to run.
		if err = c.waitForJobToBeRunnable(ctx, projectIdentifier, job.ID); err != nil {
			return nil, err
		}
	}

	ui.Output("Running GitLab CI job %q for pipeline %d", job.Name, pipelineID, terminal.WithInfoStyle())

	jobVariables := []*gitlab.JobVariableOptions{}
	for key, value := range input.EnvironmentVariables {
		jobVariables = append(jobVariables, &gitlab.JobVariableOptions{
			Key:          ptr.String(key),
			Value:        ptr.String(value),
			VariableType: ptr.String("env_var"),
		})
	}

	for key, value := range input.FileVariables {
		jobVariables = append(jobVariables, &gitlab.JobVariableOptions{
			Key:          ptr.String(key),
			Value:        ptr.String(value),
			VariableType: ptr.String("file"),
		})
	}

	token, cErr := c.tokenRetriever()
	if cErr != nil {
		return nil, fmt.Errorf("failed to retrieve auth token: %w", cErr)
	}

	_, playJobResponse, err := c.apiClient.Jobs.PlayJob(
		projectIdentifier,
		job.ID,
		&gitlab.PlayJobOptions{JobVariablesAttributes: &jobVariables},
		gitlab.WithContext(ctx),
		gitlab.WithToken(c.authType, token),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to run GitLab CI job: %w", err)
	}
	defer playJobResponse.Body.Close()

	if playJobResponse.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("play job returned unexpected status code: %d", playJobResponse.StatusCode)
	}

	finalJob, err := c.outputJobLogs(ctx, ui, projectIdentifier, job.ID)
	if err != nil {
		if ctx.Err() != nil {
			// Cancel the job as the context was canceled.
			if cErr := c.handleJobCancellation(projectIdentifier, job.ID, ui); cErr != nil {
				return nil, cErr
			}

			return nil, ctx.Err()
		}
		return nil, err
	}

	return finalJob, nil
}

// CreatePipeline creates a GitLab CI pipeline
func (c *Client) CreatePipeline(ctx context.Context, input *CreatePipelineInput, ui terminal.UI) (*gitlab.Pipeline, error) {
	projectIdentifier, err := c.getProjectIdentifier(input.ProjectPath, input.ProjectID)
	if err != nil {
		return nil, err
	}

	// Build the pipeline variables
	pipelineVariables := []*gitlab.PipelineVariableOptions{}
	for key, value := range input.EnvironmentVariables {
		pipelineVariables = append(pipelineVariables, &gitlab.PipelineVariableOptions{
			Key:          ptr.String(key),
			Value:        ptr.String(value),
			VariableType: ptr.String("env_var"),
		})
	}

	for key, value := range input.FileVariables {
		pipelineVariables = append(pipelineVariables, &gitlab.PipelineVariableOptions{
			Key:          ptr.String(key),
			Value:        ptr.String(value),
			VariableType: ptr.String("file"),
		})
	}

	token, err := c.tokenRetriever()
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve auth token: %w", err)
	}

	pipeline, createPipelineResponse, err := c.apiClient.Pipelines.CreatePipeline(
		projectIdentifier,
		&gitlab.CreatePipelineOptions{
			Ref:       &input.Ref,
			Variables: &pipelineVariables,
		},
		gitlab.WithContext(ctx),
		gitlab.WithToken(c.authType, token),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create GitLab CI pipeline: %w", err)
	}
	defer createPipelineResponse.Body.Close()

	if createPipelineResponse.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("create pipeline returned unexpected status code: %d", createPipelineResponse.StatusCode)
	}

	finalPipeline, err := c.waitForPipelineCompletion(ctx, ui, projectIdentifier, pipeline.ID)
	if err != nil {
		if ctx.Err() != nil {
			// Cancel the pipeline as the context was canceled.
			if cErr := c.handlePipelineCancellation(projectIdentifier, pipeline.ID, ui); cErr != nil {
				return nil, cErr
			}

			return nil, ctx.Err()
		}

		return nil, err
	}

	return finalPipeline, nil
}

// outputJobLogs outputs the GitLab CI job logs to the UI
func (c *Client) outputJobLogs(ctx context.Context, ui terminal.UI, projectIdentifier any, jobID int) (*gitlab.Job, error) {
	c.logger.Info("Querying GitLab CI job logs", "project", projectIdentifier, "job_id", jobID)

	ui.Output("GitLab Output Logs", terminal.WithInfoStyle())

	writer, err := ui.OutputWriter()
	if err != nil {
		return nil, err
	}
	defer writer.Close()

	var (
		// Retry fetching the logs a few times, as the rest endpoint may return a non-200 status code
		attempts int
		// Keep track of the last seen log size, so we can seek to it when fetching new logs.
		lastSeenLogSize int64
	)

	for {
		if attempts >= maxPollingAttempts {
			return nil, fmt.Errorf("failed to get job logs after %d attempts", attempts)
		}

		token, err := c.tokenRetriever()
		if err != nil {
			return nil, fmt.Errorf("failed to retrieve auth token: %w", err)
		}

		job, getJobResponse, err := c.apiClient.Jobs.GetJob(projectIdentifier, jobID, gitlab.WithContext(ctx), gitlab.WithToken(c.authType, token))
		if err != nil {
			return nil, fmt.Errorf("failed to get GitLab CI job: %w", err)
		}
		defer getJobResponse.Body.Close()

		if getJobResponse.StatusCode != http.StatusOK {
			// Retry fetching the job.
			attempts++

			c.logger.Error("Failed to get GitLab CI job", "project", projectIdentifier, "job_id", jobID, "attempt", attempts, "status_code", getJobResponse.StatusCode)

			if err = sleep(ctx, logPollingInterval); err != nil {
				return nil, fmt.Errorf("polling interrupted: %w", err)
			}

			continue
		}

		token, err = c.tokenRetriever()
		if err != nil {
			return nil, fmt.Errorf("failed to retrieve auth token: %w", err)
		}

		// Fetch the logs
		logs, getTraceFileResponse, err := c.apiClient.Jobs.GetTraceFile(projectIdentifier, jobID, gitlab.WithContext(ctx), gitlab.WithToken(c.authType, token))
		if err != nil {
			return nil, fmt.Errorf("failed to get GitLab CI job logs: %w", err)
		}
		defer getTraceFileResponse.Body.Close()

		if getTraceFileResponse.StatusCode != http.StatusOK {
			// Retry fetching the logs.
			attempts++

			c.logger.Error("Failed to get GitLab CI job logs", "project", projectIdentifier, "job_id", jobID, "attempt", attempts, "status_code", getTraceFileResponse.StatusCode)

			if err = sleep(ctx, logPollingInterval); err != nil {
				return nil, fmt.Errorf("polling interrupted: %w", err)
			}

			continue
		}

		// Seek to the last seen log size
		if _, err = logs.Seek(lastSeenLogSize, 0); err != nil {
			return nil, fmt.Errorf("failed to seek to last seen log size: %w", err)
		}

		if logs.Len() > 0 {
			// Update the last seen log size
			lastSeenLogSize = logs.Size()

			// Output the logs
			if _, err = io.Copy(writer, logs); err != nil {
				return nil, fmt.Errorf("failed to copy logs to output: %w", err)
			}
		}

		if logs.Len() == 0 && job.FinishedAt != nil {
			switch job.Status {
			case "failed", "canceled":
				return nil, fmt.Errorf("job %s failed with status %s", job.Name, job.Status)
			}

			return job, nil
		}

		// Sleep for a bit before fetching the logs again
		if err = sleep(ctx, logPollingInterval); err != nil {
			return nil, fmt.Errorf("polling interrupted: %w", err)
		}

		// Reset the attempts counter
		attempts = 0
	}
}

func (c *Client) waitForPipelineCompletion(ctx context.Context, ui terminal.UI, projectIdentifier any, pipelineID int) (*gitlab.Pipeline, error) {
	c.logger.Info("Waiting for GitLab CI pipeline completion", "project", projectIdentifier, "pipeline_id", pipelineID)

	ui.Output("GitLab Pipeline Status", terminal.WithInfoStyle())

	var (
		// Retry fetching the pipeline a few times, as the rest endpoint may return a non-200 status code
		attempts int
		// Keep track of the last seen log size, so we can seek to it when fetching new logs.
		count int
	)

	for {
		token, err := c.tokenRetriever()
		if err != nil {
			return nil, fmt.Errorf("failed to retrieve auth token: %w", err)
		}

		pipeline, getPipelineResponse, err := c.apiClient.Pipelines.GetPipeline(projectIdentifier, pipelineID, gitlab.WithContext(ctx), gitlab.WithToken(c.authType, token))
		if err != nil {
			return nil, fmt.Errorf("failed to get GitLab CI pipeline: %w", err)
		}
		defer getPipelineResponse.Body.Close()

		if getPipelineResponse.StatusCode != http.StatusOK {
			// Retry fetching the pipeline.
			attempts++

			c.logger.Error("Failed to get GitLab CI pipeline", "project", projectIdentifier, "pipeline_id", pipelineID, "attempt", attempts, "status_code", getPipelineResponse.StatusCode)

			if attempts >= maxPollingAttempts {
				return nil, fmt.Errorf("failed to get pipeline after %d attempts", attempts)
			}

			if err = sleep(ctx, statusPollingInterval); err != nil {
				return nil, fmt.Errorf("polling interrupted: %w", err)
			}

			continue
		}

		if pipeline.FinishedAt != nil {
			c.printPipelineStatus(ui, pipeline)

			switch pipeline.Status {
			case "failed", "canceled":
				return nil, fmt.Errorf("pipeline %d failed with status %s", pipelineID, pipeline.Status)
			}

			return pipeline, nil
		}

		if count%6 == 0 {
			// Print the pipeline status every minute.
			c.printPipelineStatus(ui, pipeline)
		}

		count++

		// Sleep for a bit before fetching the pipeline again
		if err = sleep(ctx, statusPollingInterval); err != nil {
			return nil, fmt.Errorf("polling interrupted: %w", err)
		}

		// Reset the attempts counter
		attempts = 0
	}
}

func (c *Client) waitForJobToBeRunnable(ctx context.Context, projectIdentifier any, jobID int) error {
	var attempts int

	// Wait until job is ready to run.
	for {
		token, err := c.tokenRetriever()
		if err != nil {
			return fmt.Errorf("failed to retrieve auth token: %w", err)
		}

		statusJob, statusJobResponse, statusErr := c.apiClient.Jobs.GetJob(
			projectIdentifier,
			jobID,
			gitlab.WithContext(ctx),
			gitlab.WithToken(c.authType, token),
		)
		if statusErr != nil {
			return fmt.Errorf("failed to get GitLab CI job: %w", statusErr)
		}
		defer statusJobResponse.Body.Close()

		if statusJobResponse.StatusCode != http.StatusOK {
			// Retry fetching the job.
			attempts++

			c.logger.Error("Failed to get GitLab CI job", "project", projectIdentifier, "job_id", jobID, "attempt", attempts, "status_code", statusJobResponse.StatusCode)

			if attempts >= maxPollingAttempts {
				return fmt.Errorf("failed to get job after %d attempts", attempts)
			}

			if err := sleep(ctx, statusPollingInterval); err != nil {
				return fmt.Errorf("polling interrupted: %w", err)
			}

			continue
		}

		switch statusJob.Status {
		case "manual":
			// Job is ready to run.
			return nil
		case "failed", "canceled", "skipped", "success", "pending", "running", "waiting_for_resource":
			// Job is not in a runnable state.
			return fmt.Errorf("job %s is not in a runnable state: job has status %s", statusJob.Name, statusJob.Status)
		}

		// Sleep for a bit before checking the job status again
		if err := sleep(ctx, statusPollingInterval); err != nil {
			return fmt.Errorf("polling interrupted: %w", err)
		}

		// Reset the attempts counter
		attempts = 0
	}
}

// getPipelineJob locates the target job in the pipeline and returns it.
func (c *Client) getPipelineJob(
	ctx context.Context,
	projectIdentifier any,
	pipelineID int,
	targetJobName string,
) (*gitlab.Job, error) {
	currentPage := 1
	for {
		token, err := c.tokenRetriever()
		if err != nil {
			return nil, fmt.Errorf("failed to retrieve auth token: %w", err)
		}

		jobs, listJobsResponse, err := c.apiClient.Jobs.ListPipelineJobs(
			projectIdentifier,
			pipelineID,
			&gitlab.ListJobsOptions{
				Scope: &[]gitlab.BuildStateValue{
					// Ensure we don't get jobs that are still pending, running, skipped, etc.
					gitlab.Canceled,
					gitlab.Failed,
					gitlab.Success,
					gitlab.Created,
					gitlab.Manual,
				},
				ListOptions: gitlab.ListOptions{
					Page: currentPage,
				},
				IncludeRetried: ptr.Bool(true),
			},
			gitlab.WithContext(ctx),
			gitlab.WithToken(c.authType, token),
		)
		if err != nil {
			return nil, fmt.Errorf("failed to get GitLab CI jobs: %w", err)
		}
		defer listJobsResponse.Body.Close()

		if listJobsResponse.StatusCode != http.StatusOK {
			return nil, fmt.Errorf("list jobs returned unexpected status code: %d", listJobsResponse.StatusCode)
		}

		if len(jobs) == 0 {
			return nil, fmt.Errorf("no runnable jobs found for pipeline %d", pipelineID)
		}

		// Find the job we want to run
		for _, j := range jobs {
			if j.Name == targetJobName {
				return j, nil
			}
		}

		// Check if we've reached the last page
		if listJobsResponse.NextPage == 0 {
			return nil, fmt.Errorf("job %q not found for pipeline %d, or job is not in a runnable state", targetJobName, pipelineID)
		}

		// Increment the page counter
		currentPage++

		// Sleep for a bit before fetching the next page
		if err = sleep(ctx, 2); err != nil {
			return nil, fmt.Errorf("polling interrupted: %w", err)
		}
	}
}

// handleJobCancellation cancels a GitLab CI job and waits for it to finish.
func (c *Client) handleJobCancellation(projectIdentifier any, jobID int, ui terminal.UI) error {
	ui.Output("Canceling GitLab CI job...", terminal.WithInfoStyle())

	token, err := c.tokenRetriever()
	if err != nil {
		return fmt.Errorf("failed to retrieve auth token: %w", err)
	}

	_, cancelJobResponse, err := c.apiClient.Jobs.CancelJob(projectIdentifier, jobID, gitlab.WithToken(c.authType, token))
	if err != nil {
		return fmt.Errorf("failed to cancel GitLab CI job: %w", err)
	}
	defer cancelJobResponse.Body.Close()

	if cancelJobResponse.StatusCode != http.StatusCreated {
		return fmt.Errorf("cancel job returned unexpected status code: %d", cancelJobResponse.StatusCode)
	}

	for {
		token, err := c.tokenRetriever()
		if err != nil {
			return fmt.Errorf("failed to retrieve auth token: %w", err)
		}

		job, getJobResponse, err := c.apiClient.Jobs.GetJob(projectIdentifier, jobID, gitlab.WithToken(c.authType, token))
		if err != nil {
			return fmt.Errorf("failed to get GitLab CI job: %w", err)
		}
		defer getJobResponse.Body.Close()

		if getJobResponse.StatusCode != http.StatusOK {
			return fmt.Errorf("get job returned unexpected status code: %d", getJobResponse.StatusCode)
		}

		switch job.Status {
		case "failed", "canceled", "skipped", "success":
			// Job is finished, so we can return.
			ui.Output("GitLab CI job canceled", terminal.WithInfoStyle())
			return nil
		}

		time.Sleep(statusPollingInterval)
	}
}

func (c *Client) handlePipelineCancellation(projectIdentifier any, pipelineID int, ui terminal.UI) error {
	ui.Output("Canceling GitLab CI pipeline...", terminal.WithInfoStyle())

	token, err := c.tokenRetriever()
	if err != nil {
		return fmt.Errorf("failed to retrieve auth token: %w", err)
	}

	_, cancelPipelineResponse, err := c.apiClient.Pipelines.CancelPipelineBuild(projectIdentifier, pipelineID, gitlab.WithToken(c.authType, token))
	if err != nil {
		return fmt.Errorf("failed to cancel GitLab CI pipeline: %w", err)
	}
	defer cancelPipelineResponse.Body.Close()

	if cancelPipelineResponse.StatusCode != http.StatusOK {
		return fmt.Errorf("cancel pipeline returned unexpected status code: %d", cancelPipelineResponse.StatusCode)
	}

	for {
		token, err := c.tokenRetriever()
		if err != nil {
			return fmt.Errorf("failed to retrieve auth token: %w", err)
		}

		pipeline, getPipelineResponse, err := c.apiClient.Pipelines.GetPipeline(projectIdentifier, pipelineID, gitlab.WithToken(c.authType, token))
		if err != nil {
			return fmt.Errorf("failed to get GitLab CI pipeline: %w", err)
		}
		defer getPipelineResponse.Body.Close()

		if getPipelineResponse.StatusCode != http.StatusOK {
			return fmt.Errorf("get pipeline returned unexpected status code: %d", getPipelineResponse.StatusCode)
		}

		switch pipeline.Status {
		case "failed", "canceled", "skipped", "success":
			// Pipeline is finished, so we can return.
			ui.Output("GitLab CI pipeline canceled", terminal.WithInfoStyle())
			return nil
		}

		time.Sleep(statusPollingInterval)
	}
}

// getProjectIdentifier returns the project identifier from the input data
func (*Client) getProjectIdentifier(projectPath *string, projectID *int) (any, error) {
	if projectPath != nil {
		return *projectPath, nil
	}

	if projectID != nil {
		return *projectID, nil
	}

	return nil, status.Errorf(codes.InvalidArgument, "project_path or project_id must be set")
}

func (*Client) printPipelineStatus(ui terminal.UI, pipeline *gitlab.Pipeline) {
	if pipeline.FinishedAt != nil {
		ui.Output(fmt.Sprintf("[%s elapsed] %s",
			humanize.RelTime(*pipeline.StartedAt, *pipeline.FinishedAt, "", ""),
			strings.ToUpper(pipeline.Status),
		), terminal.WithInfoStyle())
		return
	}

	if pipeline.StartedAt != nil {
		ui.Output(fmt.Sprintf("[%s elapsed] %s",
			humanize.RelTime(*pipeline.StartedAt, time.Now(), "", ""),
			strings.ToUpper(pipeline.Status),
		), terminal.WithInfoStyle())
		return
	}

	ui.Output("Waiting for pipeline to start... %s", strings.ToUpper(pipeline.Status), terminal.WithInfoStyle())
}

// sleep sleeps for the given duration, or until the context is done
func sleep(ctx context.Context, duration time.Duration) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-time.After(duration):
	}
	return nil
}
