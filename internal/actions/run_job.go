// Package actions contains the plugin actions provided by this plugin
package actions

import (
	"context"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-gitlab/internal/gitlab"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
)

var (
	_ pipeline.ConfigurableAction = (*runJobAction)(nil)
	_ pipeline.ActionInput        = (*RunJobActionInputData)(nil)
)

// RunJobActionResponse is the response struct returned from the action.
// These values will be exposed as action outputs so they can be consumed by
// subsequent actions in the pipeline
type RunJobActionResponse struct {
	Status string `doc:"The status of the job"`
}

// RunJobActionInputData is the input data for this action
type RunJobActionInputData struct {
	CommitID             *string           `hcl:"commit_id,optional" doc:"The commit ID to run the job on"`
	ProjectPath          *string           `hcl:"project_path,optional" doc:"The path of the project to run the job in (project_path or project_id must be set)"`
	ProjectID            *int              `hcl:"project_id,optional" doc:"The ID of the project to run the job in (project_path or project_id must be set)"`
	EnvironmentVariables map[string]string `hcl:"environment_variables,optional" doc:"The environment variables to set for the job"`
	FileVariables        map[string]string `hcl:"file_variables,optional" doc:"The file variables to set for the job"`
	Ref                  string            `hcl:"ref,attr" doc:"The ref to run the job on (e.g. branch or tag name)"`
	JobName              string            `hcl:"job_name,attr" doc:"The name of the job to run"`
}

// IsActionInput is a marker function to indicate that this struct is used for action input
func (e *RunJobActionInputData) IsActionInput() {}

// runJobAction is the action implementation
type runJobAction struct {
	gitlabClient *gitlab.Client
}

// NewRunJobAction returns a new action instance
func NewRunJobAction() pipeline.Action {
	return &runJobAction{}
}

func (a *runJobAction) Configure(data interface{}, _ hclog.Logger) error {
	a.gitlabClient = data.(*gitlab.Client)
	return nil
}

func (a *runJobAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "run_job",
		Description: "Run a GitLab CI job",
	}
}

func (a *runJobAction) ExecuteFunc() interface{} {
	return a.execute
}

func (a *runJobAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *RunJobActionInputData,
) (*RunJobActionResponse, error) {
	ui.Output("Running GitLab CI job %q on ref %q", input.JobName, input.Ref, terminal.WithInfoStyle())

	job, err := a.gitlabClient.RunJob(ctx, &gitlab.RunJobInput{
		ProjectPath:          input.ProjectPath,
		ProjectID:            input.ProjectID,
		CommitID:             input.CommitID,
		Ref:                  input.Ref,
		JobName:              input.JobName,
		EnvironmentVariables: input.EnvironmentVariables,
		FileVariables:        input.FileVariables,
	}, ui)
	if err != nil {
		return nil, err
	}

	return &RunJobActionResponse{
		Status: job.Status,
	}, nil
}
