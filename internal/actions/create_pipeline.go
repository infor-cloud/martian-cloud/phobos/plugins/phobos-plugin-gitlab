package actions

import (
	"context"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-gitlab/internal/gitlab"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/terminal"
)

var (
	_ pipeline.ConfigurableAction = (*createPipelineAction)(nil)
	_ pipeline.ActionInput        = (*CreatePipelineActionInputData)(nil)
)

// CreatePipelineActionResponse is the response struct returned from the action.
// These values will be exposed as action outputs so they can be consumed by
// subsequent actions in the pipeline
type CreatePipelineActionResponse struct {
	Status string `doc:"The status of the pipeline"`
	ID     int    `doc:"The ID of the pipeline"`
}

// CreatePipelineActionInputData is the input data for this action
type CreatePipelineActionInputData struct {
	ProjectPath          *string           `hcl:"project_path,optional" doc:"The path of the project to create the pipeline in (project_path or project_id must be set)"`
	ProjectID            *int              `hcl:"project_id,optional" doc:"The ID of the project to create the pipeline in (project_path or project_id must be set)"`
	EnvironmentVariables map[string]string `hcl:"environment_variables,optional" doc:"The environment variables to set for the pipeline"`
	FileVariables        map[string]string `hcl:"file_variables,optional" doc:"The file variables to set for the pipeline"`
	Ref                  string            `hcl:"ref,attr" doc:"The ref to create the pipeline on (e.g. branch or tag name)"`
}

// IsActionInput is a marker function to indicate that this struct is used for action input
func (e *CreatePipelineActionInputData) IsActionInput() {}

// createPipelineAction is the action implementation
type createPipelineAction struct {
	gitlabClient *gitlab.Client
}

// NewCreatePipelineAction returns a new action instance
func NewCreatePipelineAction() pipeline.Action {
	return &createPipelineAction{}
}

func (a *createPipelineAction) Configure(data interface{}, _ hclog.Logger) error {
	a.gitlabClient = data.(*gitlab.Client)
	return nil
}

func (a *createPipelineAction) Metadata() *pipeline.ActionMetadata {
	return &pipeline.ActionMetadata{
		Name:        "create_pipeline",
		Description: "Create a GitLab CI pipeline",
	}
}

func (a *createPipelineAction) ExecuteFunc() interface{} {
	return a.execute
}

func (a *createPipelineAction) execute(
	ctx context.Context,
	ui terminal.UI,
	input *CreatePipelineActionInputData,
) (*CreatePipelineActionResponse, error) {
	ui.Output("Creating GitLab CI pipeline on ref %s", input.Ref, terminal.WithInfoStyle())

	pipeline, err := a.gitlabClient.CreatePipeline(ctx, &gitlab.CreatePipelineInput{
		ProjectPath:          input.ProjectPath,
		ProjectID:            input.ProjectID,
		Ref:                  input.Ref,
		EnvironmentVariables: input.EnvironmentVariables,
		FileVariables:        input.FileVariables,
	}, ui)
	if err != nil {
		return nil, err
	}

	return &CreatePipelineActionResponse{
		ID:     pipeline.ID,
		Status: pipeline.Status,
	}, nil
}
