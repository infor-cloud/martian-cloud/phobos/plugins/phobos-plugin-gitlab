// Package plugin contains the Phobos GitLab pipeline plugin.
package plugin

import (
	"fmt"
	"os"

	"github.com/hashicorp/go-hclog"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-gitlab/internal/actions"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-gitlab/internal/gitlab"
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/framework/pipeline"
)

var _ pipeline.Plugin = (*Plugin)(nil)

// Config defines the required config fields for this pipeline.
// HCL tags are used for defining the accepted fields https://pkg.go.dev/githup.com/hashicorp/hcl/v2/gohcl#pkg-overview
type Config struct {
	APIURL    *string `hcl:"api_url,optional" doc:"The URL of the GitLab API"`
	Token     *string `hcl:"token,optional" doc:"The GitLab API token"`
	TokenFile *string `hcl:"token_file,optional" doc:"The path to a file containing the GitLab API token"`
	AuthType  *string `hcl:"auth_type,optional" doc:"The type of authentication to use (access_token or oauth_token), defaults to access_token"`
}

// Plugin is the Phobos GitLab pipeline plugin.
type Plugin struct {
	config  Config
	Version string
}

// Config returns the config struct for this plugin
func (p *Plugin) Config() (interface{}, error) {
	// Config values will be populated automatically
	return &p.config, nil
}

// ValidateConfig validates the config struct for this plugin
func (p *Plugin) ValidateConfig(_ interface{}) error {
	if p.config.Token == nil && p.config.TokenFile == nil {
		return fmt.Errorf("either token or token_file must be provided")
	}

	if p.config.AuthType != nil && *p.config.AuthType != string(gitlab.AccessToken) && *p.config.AuthType != string(gitlab.OAuthToken) {
		return fmt.Errorf("auth_type must be either access_token or oauth_token, got %s", *p.config.AuthType)
	}

	return nil
}

// PluginData returns data that can be consumed by actions
func (p *Plugin) PluginData(logger hclog.Logger) (interface{}, error) {
	configArgs := []interface{}{}

	if p.config.APIURL != nil {
		configArgs = append(configArgs, "api_url", *p.config.APIURL)
	} else {
		configArgs = append(configArgs, "api_url", "undefined")
	}

	var tokenRetriever func() (string, error)
	if p.config.Token != nil {
		tokenRetriever = func() (string, error) { return *p.config.Token, nil }
		configArgs = append(configArgs, "token", "using provided token")
	}

	if p.config.TokenFile != nil {
		tokenRetriever = func() (string, error) {
			buffer, err := os.ReadFile(*p.config.TokenFile)
			if err != nil {
				return "", fmt.Errorf("failed to read token file: %w", err)
			}
			return string(buffer), nil
		}

		configArgs = append(configArgs, "token_file", *p.config.TokenFile)
	}

	authType := gitlab.AccessToken
	if p.config.AuthType != nil {
		authType = gitlab.AuthType(*p.config.AuthType)
	}

	configArgs = append(configArgs, "auth_type", authType)

	logger.Info(
		"plugin configuration:",
		configArgs...,
	)

	return gitlab.New(&gitlab.Options{
		Version:        p.Version,
		APIURL:         p.config.APIURL,
		TokenRetriever: tokenRetriever,
		AuthType:       authType,
		Logger:         logger,
	})
}

// GetActions returns the actions that this plugin supports
func (p *Plugin) GetActions() []func() pipeline.Action {
	return []func() pipeline.Action{
		actions.NewRunJobAction,
		actions.NewCreatePipelineAction,
	}
}
