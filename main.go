// Package main contains the initiation module for the Phobos GitLab plugin.
package main

import (
	"gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-gitlab/internal/plugin"
	sdk "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk"
)

//go:generate go run gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/cmd schema
//go:generate go run gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-docs/cmd generate

// Version is passed in via ldflags at build time
var Version = "dev"

func main() {
	sdk.Main(
		sdk.WithPluginDescription("A plugin for interacting with GitLab"),
		sdk.WithPipelinePlugin(&plugin.Plugin{Version: Version}),
	)
}
