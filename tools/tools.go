//go:build tools

package tools

import (
	// Documentation generation
	_ "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-docs/cmd"
	_ "gitlab.com/infor-cloud/martian-cloud/phobos/phobos-plugin-sdk/cmd"
)
